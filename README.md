# DFL::Settings

The Settings library is the more sophisticated version of QSettings. It emits a signal whenever a setting is changed.
This helps in applying the setings "live", i.e., without restarting the app. Additionally, it also takes care of default
settings and distro settings. Similar to QSettings, DFL::Settings also allows the user to set custom settings backends.


### Dependencies:
* <tt>Qt5 Core and Gui (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/settings.git dfl-settings`
- Enter the `dfl-settings` folder
  * `cd dfl-settings`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Group access functions like hasGroup, beginGroup, endGroup, removeGroup, etc
* Verify dynamic key/group mapping
