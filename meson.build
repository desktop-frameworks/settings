project(
	'DFL Settings',
	'cpp',
	version: '0.2.0',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_project_arguments( '-DPROJECT_VERSION=' + meson.project_version(), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

if get_option('use_qt_version') == 'qt5'
	Qt = import( 'qt5' )
	QtCore    = dependency( 'qt5', modules: [ 'Core' ] )
    QtGui     = dependency( 'qt5', modules: [ 'Gui' ] )

	libname   = 'df5settings'
	subdirname = 'DFL/DF5'

elif get_option('use_qt_version') == 'qt6'
	Qt = import( 'qt6' )
	QtCore    = dependency( 'qt6', modules: [ 'Core' ] )
    QtGui     = dependency( 'qt6', modules: [ 'Gui' ] )

	libname   = 'df6settings'
	subdirname = 'DFL/DF6'

endif

Deps = [ QtCore, QtGui ]

Headers = [
	'DFSettings.hpp',
	'SettingsImpl.hpp',
]

Sources = [
	'Settings.cpp',
	'SettingsImpl.cpp',
]

Mocs = Qt.compile_moc(
 	headers : Headers,
	dependencies: Deps
)

settings = shared_library(
	libname, [ Sources, Mocs ],
	version: meson.project_version(),
	dependencies: Deps,
	install: true,
	install_dir: get_option( 'libdir' )
)

exec = executable(
	'sett-test', [ 'test.cpp' ],
	dependencies: [QtCore, QtGui],
	link_with: settings,
	install: false
)

install_headers( 'DFSettings.hpp', subdir: subdirname )

## PkgConfig Section
pkgconfig = import( 'pkgconfig' )
pkgconfig.generate(
	settings,
	name: libname,
	subdirs: subdirname,
 	version: meson.project_version(),
	filebase: libname,
	description: 'DFL::Settings class extends the functionality of QSettings',
)
