/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Settings expands the QSettings class.
 *
 * IMPL Class
 *  This class will read the various settings files
 *  The class structure and working is inspired by QSettings.
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#include <cmath>

#include <QFontDatabase>
#include <QStandardPaths>
#include <QGuiApplication>
#include <QProcess>
#include <QScreen>
#include <QThread>
#include <QDebug>
#include <QDir>

#include "SettingsImpl.hpp"

static inline QString longestMatch( QString text, QStringList keys ) {
    /** Get a list of all keys that the text starts with. */
    QStringList matches;

    for ( QString key: keys ) {
        if ( text.startsWith( key ) ) {
            matches << key;
        }
    }

    /**
     * Alphabetical case-sensitive sort. This ensures that the longest
     * string will be at the very last.
     */
    matches.sort();

    /** Return the last match */
    if ( matches.length() ) {
        return matches.takeLast();
    }

    return QString();
}


static inline QVariantMap linearise( QVariantMap map, QString parent = QString() ) {
    QVariantMap linearMap;

    for ( QString key: map.keys() ) {
        QString newKey = (parent.size() ? parent + "::" + key : key);

        if ( (QMetaType::Type)map[ key ].type() == QMetaType::QVariantMap ) {
            linearMap.insert( linearise( map[ key ].toMap(), newKey ) );
        }

        else {
            linearMap[ newKey ] = map[ key ];
        }
    }

    return linearMap;
}


static inline QVariantMap delinearise( const QVariantMap& linearMap ) {
    QVariantMap nestedMap;

    // Iterate over the keys of the linear map
    for (auto it = linearMap.constBegin(); it != linearMap.constEnd(); ++it) {
        QStringList keys  = it.key().split( "::" );
        QVariant    value = it.value();

        QVariantMap *currentMap = &nestedMap;

        // Iterate through the key parts
        for (int i = 0; i < keys.size(); ++i) {
            const QString& key = keys[ i ];

            if ( i == keys.size() - 1 ) {
                // Base case: Insert the final value
                (*currentMap)[ key ] = value;
            }
            else {
                // Intermediate case: Create or retrieve the nested map
                if ( !(*currentMap)[ key ].canConvert<QVariantMap>() ) {
                    (*currentMap)[ key ] = QVariantMap();
                }

                // Update currentMap to point to the nested map
                currentMap = reinterpret_cast<QVariantMap *>(&( (*currentMap)[ key ]) );
            }
        }
    }

    return nestedMap;
}


static inline bool isTouchCapable() {
    QProcess proc;

    proc.start( "udevadm", { "info", "--export-db" } );
    proc.waitForFinished( -1 );

    QString output = QString::fromLocal8Bit( proc.readAllStandardOutput() + '\n' + proc.readAllStandardError() );

    if ( output.contains( "ID_INPUT_TOUCHSCREEN=1" ) ) {
        return true;
    }

    return false;
}


static inline int getFormFactor() {
    /**
     * Form Factors
     * ------------
     *
     * 0: Desktop - Typically 14" to 16" screens
     * 1: Tablets - Typically  7" to 10" screens
     * 2: Mobiles - Typically  4" to  6" screens
     * 3: Monitor - Typically larger than 16" - huge sizes, we're not built for these screens.
     **/

    QSizeF screenSize = QGuiApplication::primaryScreen()->physicalSize();
    double diag       = sqrt( pow( screenSize.width(), 2 ) + pow( screenSize.height(), 2 ) ) / 25.4;

    //- Deady large screen mobiles
    if ( diag <= 6.5 ) {
        return 2;
    }

    //- Tablets and small netbooks
    else if ( diag <= 10.1 ) {
        return 1;
    }

    //- Typical desktops
    else if ( diag <= 16.0 ) {
        return 0;
    }

    // Large displays
    else {
        return 3;
    }

    /* Default value */
    return 0;
}


DFL::Impl::Settings::Settings( QString project, QString app, QString name ) {
    mProjectName     = project;
    mAppName         = app;
    mSettingFileName = name;

    watcher = new QFileSystemWatcher( this );

    /** Reload the settings no matter which file changes */
    connect( watcher, &QFileSystemWatcher::fileChanged, this, &DFL::Impl::Settings::getChangedSettings );

    connect(
        watcher, &QFileSystemWatcher::directoryChanged, [ = ] ( QString cfgDir ) {
            if ( cfgDir == QFileInfo( defCfgPath ).path() ) {
                getChangedSettings( defCfgPath );
            }

            else if ( cfgDir == QFileInfo( sysCfgPath ).path() ) {
                getChangedSettings( sysCfgPath );
            }

            else if ( cfgDir == QFileInfo( usrCfgPath ).path() ) {
                /** Add usrCfgPath only if it's not being watched */
                getChangedSettings( usrCfgPath );
            }
        }
    );
}


void DFL::Impl::Settings::registerDynamicGroup( QString group ) {
    initialize();

    mDynamicGroups << group;
    readCurrentSettings();
}


void DFL::Impl::Settings::deregisterDynamicGroup( QString group ) {
    initialize();

    mDynamicGroups.removeAll( group );
    readCurrentSettings();
}


void DFL::Impl::Settings::registerDynamicKey( QString key ) {
    initialize();

    mDynamicKeys << key;
    readCurrentSettings();
}


void DFL::Impl::Settings::deregisterDynamicKey( QString key ) {
    initialize();

    mDynamicKeys.removeAll( key );
    readCurrentSettings();
}


void DFL::Impl::Settings::setIgnoreChangesToDefault( bool yes ) {
    mIgnoreChangesToDefaults = yes;
}


QVariant DFL::Impl::Settings::value( const QString& key ) {
    initialize();

    if ( mInvalidKeys.contains( key ) ) {
        return QVariant();
    }

    /** Easy: The user config contains this key */
    if ( usrSett.contains( key ) ) {
        return usrSett[ key ];
    }

    /** Okay, may be distro settings has it */
    else if ( sysSett.contains( key ) ) {
        return sysSett[ key ];
    }

    /** Alright default settings gotta have it */
    else if ( defSett.contains( key ) ) {
        return defSett[ key ];
    }

    /** Oh! So this might be a dynamic key */
    else if ( isDynamicKey( key ) ) {
        /** From a dynamic group */
        for ( QString dynGroupBase: mDynamicGroups ) {
            /**
             * So this is a key from a dynamic group
             * without a user defined value. We will return
             * a default value.
             */
            if ( key.startsWith( dynGroupBase + mDynSep ) ) {
                QStringList dynParts = key.split( mDynSep );
                dynParts = dynParts[ 1 ].split( "::" );

                QString dynPart = dynParts[ 0 ];

                return value( QString( key ).replace( mDynSep + dynPart, "" ) );
            }
        }

        /** So, it's a pure dynamic key */
        for ( QString dynKeyBase: mDynamicKeys ) {
            /**
             * So this is a dynamic key without a user defined value.
             * We will return a default value.
             */
            if ( key.startsWith( dynKeyBase + mDynSep ) ) {
                return value( dynKeyBase );
            }
        }
    }

    return QVariant();
}


bool DFL::Impl::Settings::setValue( const QString& key, QVariant value ) {
    initialize();

    /** Irrespective of whether the key is valid or not */
    usrSett[ key ] = value;

    /** Write the user settings to file */
    QVariantMap _map = delinearise( usrSett );
    return backend->wFn( _map, usrCfgPath );
}


QStringList DFL::Impl::Settings::allKeys() {
    initialize();

    QStringList keys = defSett.keys();
    for ( QString key: usrSett.keys() ) {
        if ( keys.contains( key ) == false ) {
            keys << key;
        }
    }

    return keys;
}


bool DFL::Impl::Settings::hasKey( QString key ) {
    initialize();

    if ( mInvalidKeys.contains( key ) ) {
        return false;
    }

    return defSett.contains( key ) || usrSett.contains( key );
}


bool DFL::Impl::Settings::remove( QString key ) {
    initialize();

    return usrSett.remove( key );
}


bool DFL::Impl::Settings::initializeBackend( SettingsBackend backend ) {
    /** Should be called before caling data-access functions */
    if ( mInitComplete ) {
        return false;
    }

    this->backend = new DFL::Impl::SettingsBackend();

    this->backend->name = backend.name;
    this->backend->ext  = backend.ext;
    this->backend->rFn  = backend.rFn;
    this->backend->wFn  = backend.wFn;

    return true;
}


bool DFL::Impl::Settings::setConfigPath( DFL::Settings::ConfigScope scope, QString cfgPath ) {
    /** Should be called before caling data-access functions */
    if ( mInitComplete ) {
        return false;
    }

    /** Obtain the actual path, from the path spec */
    cfgPath.replace( "@p", mProjectName.toLower() );
    cfgPath.replace( "@a", mAppName.toLower() );
    cfgPath.replace( "@s", mSettingFileName.toLower() );
    cfgPath.replace( "@P", mProjectName );
    cfgPath.replace( "@A", mAppName );
    cfgPath.replace( "@S", mSettingFileName );

    QString   cfgDir = QFileInfo( cfgPath ).absolutePath();
    QFileInfo pathInfo( cfgDir );

    /** The given path has to be a directory */
    if ( pathInfo.isDir() == false ) {
        return false;
    }

    /** The given path has to be readable, which translates to rx permissions */
    if ( (pathInfo.isReadable() == false) or (pathInfo.isExecutable() == false) ) {
        return false;
    }

    switch ( scope ) {
        /** The path should be writable for user scope. */
        case DFL::Settings::UserScope: {
            if ( pathInfo.isWritable() ) {
                mConfigPaths[ scope ] = cfgPath;
                return true;
            }

            else {
                return false;
            }
        }

        /** For others, we already know it's readable. Set it and skedaddle. */
        default: {
            mConfigPaths[ scope ] = cfgPath;
            return true;
        }
    }

    /** Return false by default */
    return false;
}


void DFL::Impl::Settings::initialize() {
    /** Initializations are complete, nothing to be done. */
    if ( mInitComplete ) {
        return;
    }

    /** If the backend is not ready, initialize it to the default backend */
    if ( backend == nullptr ) {
        initializeDefaultBackend();
    }

    /** Prepare the config paths: only for scoped settings */
    if ( mProjectName.length() && mAppName.length() ) {
        defCfgPath = mConfigPaths[ DFL::Settings::SystemScope ].replace( ".ext", "." + backend->ext );
        sysCfgPath = mConfigPaths[ DFL::Settings::DistroScope ].replace( ".ext", "." + backend->ext );
        usrCfgPath = mConfigPaths[ DFL::Settings::UserScope ].replace( ".ext", "." + backend->ext );

        if ( defCfgPath.length() ) {
            watcher->addPath( QFile::exists( defCfgPath ) ? defCfgPath : QFileInfo( defCfgPath ).path() );
        }

        if ( sysCfgPath.length() ) {
            watcher->addPath( QFile::exists( sysCfgPath ) ? sysCfgPath : QFileInfo( sysCfgPath ).path() );
        }
    }

    else {
        usrCfgPath = mSettingFileName;
    }

    /** Add the user config file if it exists, else add the parent dir */
    if ( QFile::exists( usrCfgPath ) ) {
        watcher->addPath( QFile::exists( usrCfgPath ) ? usrCfgPath : QFileInfo( usrCfgPath ).path() );
    }

    /** Get the current settings */
    readCurrentSettings();

    mInitComplete = true;
}


void DFL::Impl::Settings::readCurrentSettings() {
    if ( QFile::exists( usrCfgPath ) ) {
        usrSett = linearise( backend->rFn( usrCfgPath ) );
    }

    /** Clear invalid keys */
    mInvalidKeys.clear();

    /** Load scoped settings only if we're handling scoped settings */
    if ( mProjectName.length() and mAppName.length() ) {
        /** Read the settings from default config */
        if ( QFile::exists( defCfgPath ) == false ) {
            qWarning() << "The default config file is inaccessible. No settings loaded.";
            return;
        }

        defSett = linearise( backend->rFn( defCfgPath ) );

        if ( QFile::exists( sysCfgPath ) ) {
            sysSett = linearise( backend->rFn( sysCfgPath ) );
        }

        for ( QString key: sysSett.keys() ) {
            /** If we have an invalid (key, value) pair, remove it */
            if ( (defSett.contains( key ) == false) && (isDynamicKey( key ) == false) ) {
                sysSett.remove( key );
            }
        }

        for ( QString key: usrSett.keys() ) {
            /** Session settings */
            if ( key.startsWith( "Session::" ) ) {
                continue;
            }

            /** If we have an invalid (key, value) pair, tag it */
            if ( (defSett.contains( key ) == false) && (isDynamicKey( key ) == false) ) {
                mInvalidKeys << key;
            }
        }
    }
}


void DFL::Impl::Settings::initializeDefaultBackend() {
    backend       = new DFL::Impl::SettingsBackend();
    backend->name = "ini";
    backend->ext  = "conf";

    backend->rFn =
        [ = ]( QString& filename ) -> QVariantMap {
            QSettings   sett( filename, QSettings::IniFormat );
            QVariantMap _map;

            /** Get (key, value) pairs from under [General] */
            for ( QString key: sett.childKeys() ) {
                _map[ key ] = sett.value( key );
            }

            /** Get (key, value) pairs from under other groups */
            for ( QString group: sett.childGroups() ) {
                QVariantMap map2;
                sett.beginGroup( group );
                for ( QString key: sett.childKeys() ) {
                    map2[ key ] = sett.value( key );
                }
                sett.endGroup();

                _map[ group ] = map2;
            }

            return _map;
        };

    backend->wFn =
        [ = ]( QVariantMap& _map, QString& filename ) -> bool {
            QSettings sett( filename, QSettings::IniFormat );

            for ( QString key: _map.keys() ) {
                if ( (QMetaType::Type)_map[ key ].type() == QMetaType::QVariantMap ) {
                    QVariantMap map2 = _map[ key ].toMap();
                    sett.beginGroup( key );
                    for ( QString key2: map2.keys() ) {
                        sett.setValue( key2, map2[ key2 ] );
                    }
                    sett.endGroup();
                }

                else {
                    sett.setValue( key, _map[ key ] );
                }
            }

            sett.sync();

            return true;
        };
}


bool DFL::Impl::Settings::isDynamicKey( QString key ) {
    for ( QString dynKeyBase: mDynamicKeys ) {
        if ( key.startsWith( dynKeyBase + mDynSep ) ) {
            return true;
        }
    }

    for ( QString dynGroupBase: mDynamicGroups ) {
        if ( key.startsWith( dynGroupBase + mDynSep ) ) {
            return true;
        }
    }

    return false;
}


void DFL::Impl::Settings::getChangedSettings( QString file ) {
    /** App defaults file was changed */
    if ( (file == defCfgPath) && QFile::exists( file ) ) {
        /** We are to ignore changes to defaults */
        if ( mIgnoreChangesToDefaults ) {
            /** Inform the user to restart the app */
            emit restartApp();

            /** and get out */
            return;
        }

        /** Okay, we're allowed to convey the changes */
        QVariantMap added;
        QStringList removed;

        QVariantMap changed;

        /** Get the new settings */
        QVariantMap newDefSett = linearise( backend->rFn( file ) );

        /** Get the newly added keys */
        for ( QString key: newDefSett.keys() ) {
            if ( defSett.contains( key ) == false ) {
                added[ key ] = newDefSett[ key ];
                continue;
            }
        }

        /** Get the removed keys, and the changed values */
        for ( QString key: defSett.keys() ) {
            if ( newDefSett.contains( key ) == false ) {
                removed << key;
                continue;
            }

            if ( newDefSett[ key ] != defSett[ key ] ) {
                changed[ key ] = newDefSett[ key ];
            }
        }

        for ( QString key: changed.keys() ) {
            /** Distro or user settings has this key */
            if ( usrSett.contains( key ) || sysSett.contains( key ) ) {
                changed.remove( key );
                continue;
            }
        }

        /** Update defSett map */
        defSett = newDefSett;

        /** Emit the changes */
        emit settingsChanged( added, changed, removed );
    }

    /** Distro defaults file was changed */
    else if ( (file == sysCfgPath) && QFile::exists( file ) ) {
        /** We are to ignore changes to defaults */
        if ( mIgnoreChangesToDefaults ) {
            /** Inform the user to restart the app */
            emit restartApp();

            /** and get out */
            return;
        }

        /** Okay, we're allowed to convey the changes */
        QVariantMap changed;

        /** Get the new settings */
        QVariantMap newSysSett = linearise( backend->rFn( file ) );

        /** Get the newly added keys */
        for ( QString key: newSysSett.keys() ) {
            /** Prune invalid keys */
            if ( (defSett.contains( key ) == false) && (isDynamicKey( key ) == false) ) {
                newSysSett.remove( key );
            }

            /** Key added to Distro defaults */
            if ( sysSett.contains( key ) == false ) {
                /** User settings does not have this key */
                if ( usrSett.contains( key ) == false ) {
                    /** Different from app defaults. So it will appear as changed */
                    if ( defSett[ key ] != newSysSett[ key ] ) {
                        changed[ key ] = newSysSett[ key ];
                        continue;
                    }
                }
            }
        }

        /** Get the removed keys, and the changed values */
        for ( QString key: sysSett.keys() ) {
            /** This key was removed from the new distro defaults */
            if ( newSysSett.contains( key ) == false ) {
                /** And the user settings does not have it. */
                if ( usrSett.contains( key ) == false ) {
                    /** Also, it's different from the app defaults */
                    if ( defSett[ key ] != newSysSett[ key ] ) {
                        changed[ key ] = newSysSett[ key ];
                        continue;
                    }
                }
            }

            /** Not removed, but value may have changed */
            if ( newSysSett[ key ] != sysSett[ key ] ) {
                /** And the user settings does not have it. */
                if ( usrSett.contains( key ) == false ) {
                    /** Also, it's different from the app defaults */
                    if ( defSett[ key ] != newSysSett[ key ] ) {
                        changed[ key ] = newSysSett[ key ];
                        continue;
                    }
                }
            }
        }

        /** Update the sysSett map */
        sysSett = newSysSett;

        /** Emit the changes */
        emit settingsChanged( QVariantMap(), changed, QStringList() );
    }

    /** User config file was changed */
    else if ( (file == usrCfgPath) && QFile::exists( file ) ) {
        /** Okay, we're allowed to convey the changes */
        QVariantMap changed;

        /** Get the new settings */
        QVariantMap newUsrSett = linearise( backend->rFn( file ) );

        mInvalidKeys.clear();

        /** Get the newly added keys */
        for ( QString key: newUsrSett.keys() ) {
            /** Prune invalid keys */
            if ( (defSett.contains( key ) == false) && (isDynamicKey( key ) == false) ) {
                mInvalidKeys.append( key );
            }

            /** Key added to user config */
            if ( usrSett.contains( key ) == false ) {
                /** Check if it's value has changed since the last time */
                if ( value( key ) != newUsrSett[ key ] ) {
                    changed[ key ] = newUsrSett[ key ];
                }
            }
        }

        /** Get the removed keys, and the changed values */
        for ( QString key: usrSett.keys() ) {
            /** This key was removed from the new user config */
            if ( newUsrSett.contains( key ) == false ) {
                /** Do not bother about dynamic keys */
                if ( isDynamicKey( key ) ) {
                    continue;
                }

                changed[ key ] = (sysSett.contains( key ) ? sysSett[ key ] : defSett[ key ]);
            }

            /** Not removed, but value may have changed */
            if ( newUsrSett[ key ] != usrSett[ key ] ) {
                changed[ key ] = newUsrSett[ key ];
                continue;
            }
        }

        /** Update the usrSett map */
        usrSett = newUsrSett;

        /** Emit the changes */
        emit settingsChanged( QVariantMap(), changed, QStringList() );
    }
}
