/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Settings class extends the QSettings class.
 **/

#include <QGuiApplication>
#include <QFontDatabase>
#include <QLibraryInfo>
#include <QFileInfo>
#include <QScreen>
#include <QDebug>
#include <QFile>
#include <QDir>

#include "DFSettings.hpp"
#include "SettingsImpl.hpp"

/** Container to store all the registered backends */
static DFL::Impl::SettingsBackends cBackends;

/** Container to store global config paths */
static QMap<int, QString> cConfigPaths {
    { DFL::Settings::SystemScope, "/usr/share/@p/@A/@S.conf" },
    { DFL::Settings::DistroScope, "/etc/xdg/@p/@A/@S.conf" },
    { DFL::Settings::UserScope, QDir::home().filePath( ".config/@P/@A/@S.conf" ) },
};

DFL::Settings::Settings( QString fileName ) {
    /** Non-scoped settings backend object */
    impl = new DFL::Impl::Settings( "", "", fileName );
}


DFL::Settings::Settings( QString project, QString app, QString name ) {
    impl = new DFL::Impl::Settings( project, app, name );

    /** Set the config paths */
    impl->setConfigPath( UserScope,   cConfigPaths[ UserScope ] );
    impl->setConfigPath( DistroScope, cConfigPaths[ DistroScope ] );
    impl->setConfigPath( SystemScope, cConfigPaths[ SystemScope ] );

    /** Simply forward the restartApp singal. */
    connect( impl, &DFL::Impl::Settings::restartApp, this, &DFL::Settings::restartApp );

    /** Emit the additions/modifications/deletions one by one. */
    connect(
        impl, &DFL::Impl::Settings::settingsChanged, [ = ] ( QVariantMap added, QVariantMap changed, QStringList removed ) {
            for ( QString key: added.keys() ) {
                emit settingAdded( key, added[ key ] );
            }

            for ( QString key: changed.keys() ) {
                emit settingChanged( key, changed[ key ] );
            }

            for ( QString key: removed ) {
                emit settingRemoved( key );
            }
        }
    );
}


bool DFL::Settings::registerBackend( QString name, QString ext, ReadFunc rfn, WriteFunc wfn ) {
    /** Do not allow duplicates. */
    if ( cBackends.contains( name ) ) {
        qWarning() << "De-register the backend" << name << "before registering again";
        return false;
    }

    DFL::Impl::SettingsBackend backend {
        name,
        ext,
        rfn,
        wfn
    };

    cBackends[ name ] = backend;

    return true;
}


bool DFL::Settings::deregisterBackend( QString name ) {
    return cBackends.remove( name );
}


QStringList DFL::Settings::registeredBackends() {
    return cBackends.keys();
}


QString DFL::Settings::extensionForBackend( QString backend ) {
    if ( cBackends.contains( backend ) ) {
        return cBackends[ backend ].ext;
    }

    return QString();
}


void DFL::Settings::setGlobalConfigPath( ConfigScope scope, QString cfgPath ) {
    cConfigPaths[ scope ] = cfgPath;
}


bool DFL::Settings::testConfigPath( ConfigScope scope, QString project, QString app ) {
    QString cfgPath = cConfigPaths[ scope ];

    cfgPath = cfgPath.replace( "@p", project.toLower() );
    cfgPath = cfgPath.replace( "@a", app.toLower() );
    cfgPath = cfgPath.replace( "@P", project );
    cfgPath = cfgPath.replace( "@A", app );

    QString   cfgDir = QFileInfo( cfgPath ).absolutePath();
    QFileInfo pathInfo( cfgDir );

    /** The given path has to be a directory */
    if ( not pathInfo.isDir() ) {
        return false;
    }

    /** The given path has to be readable, which translates to rx permissions */
    if ( (not pathInfo.isReadable() ) or (not pathInfo.isExecutable() ) ) {
        return false;
    }

    switch ( scope ) {
        /** The path should be writable for user scope. */
        case UserScope: {
            if ( pathInfo.isWritable() ) {
                return true;
            }

            else {
                return false;
            }
        }

        /** For others, we already know it's readable. Set it and skedaddle. */
        default: {
            return true;
        }
    }

    /** Return false by default */
    return false;
}


bool DFL::Settings::setBackend( QString backend ) {
    if ( cBackends.contains( backend ) ) {
        return impl->initializeBackend( cBackends[ backend ] );
    }

    return false;
}


bool DFL::Settings::setConfigPath( ConfigScope scope, QString path ) {
    return impl->setConfigPath( scope, path );
}


void DFL::Settings::registerDynamicGroup( QString group ) {
    impl->registerDynamicGroup( group );
}


void DFL::Settings::deregisterDynamicGroup( QString grp ) {
    impl->deregisterDynamicGroup( grp );
}


void DFL::Settings::registerDynamicKey( QString key ) {
    impl->registerDynamicKey( key );
}


void DFL::Settings::deregisterDynamicKey( QString key ) {
    impl->deregisterDynamicKey( key );
}


void DFL::Settings::setIgnoreChangesToDefault( bool yes ) {
    impl->setIgnoreChangesToDefault( yes );
}


DFL::Settings::Proxy DFL::Settings::value( const QString& key ) {
    return Proxy{ impl->value( key ) };
}


bool DFL::Settings::setValue( const QString& key, QVariant value ) {
    bool ret = impl->setValue( key, value );

    /** Signal only if it's a valid key */
    if ( impl->hasKey( key ) ) {
        emit settingChanged( key, value );
        return ret;
    }

    return false;
}


QStringList DFL::Settings::allKeys() const {
    return impl->allKeys();
}


bool DFL::Settings::hasKey( const QString& key ) const {
    return impl->hasKey( key );
}


bool DFL::Settings::remove( const QString& key ) const {
    return impl->remove( key );
}
