/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Settings expands the QSettings class.
 * For use with DFL.
 **/

#pragma once

#include <QString>
#include <QMap>
#include <QSize>
#include <QFont>
#include <QRect>
#include <QVariant>
#include <QSettings>
#include <QFileSystemWatcher>

namespace DFL {
    class Settings;

    namespace Impl {
        class Settings;
    }

    typedef std::function<QVariantMap (QString&)>          ReadFunc;
    typedef std::function<bool (QVariantMap&, QString&)>   WriteFunc;
}

class DFL::Settings : public QObject {
    Q_OBJECT;

    public:
        enum ConfigScope {
            UserScope = 0x999890,    // User defined config files
            DistroScope,             // Defaults shipped by distros
            SystemScope,             // Defaults shipped by the app
        };

        /**
         * Create a generic DFL::Settings object with default file locations.
         * @filename is the name of the settings file.
         * The backend to be used will be auto-determined: custom backend will
         * be used if the backend-extension matches the extension of the file.
         * By default, this object will use the QSettings backend, and assumed
         * to be of INI format.
         *
         * Custom backends can be registered using the registerBackend(...)
         * function and registered backends can be queried using registeredBackends()
         * Set a suitable settings backend using setBackend(...) function.
         *
         * This instance will not read any scoped-settings.
         */
        Settings( QString filename );

        /**
         * Create a DFL::Settings object for an app of a project.
         * @project -> Project like DesQ, Paper, Plasma, etc..
         * @app -> Settings of a specific app: Like Files, Term, Shell, etc...
         * @name -> Name of the settings file. DO NOT include an extension.
         * An extension will be automatically added when a format is specified.
         * The default format is INI; default extension is '.conf'.
         * By default, this object will use the QSettings backend.
         *
         * Custom backends can be registered using the registerBackend(...)
         * function and registered backends can be queried using registeredBackends()
         * Set the settings backend using setBackend(...) function.
         *
         * Config file locations can be modified using setConfigPath(...)
         * Call this function before calling the data access functions.
         * Once the keys or values are accessed, the paths will not be changed.
         *
         * Default Paths:
         * System Config: /usr/share/@project/@app/@name.ext
         * Distro Config: /etc/xdg/@project/@app/@name.ext
         * User Config:   ~/.config/@project/@app/@name.ext
         */
        Settings( QString project, QString app, QString name );

        /**
         * Register a custom backend with name @name. The extension used for the config files
         * will be @ext. DO NOT include a '.' in the extention. It will be added automatically.
         * @ReadFunc is typedef for a pointer to a function with the following signature:
         * QVariantMap readConfigFromFile( QString filename );
         * @WriteFunc is typedef for a pointer to a function with the following signature:
         * bool writeConfigToFile( QVariantMap settingMap, QString filename );
         */
        static bool registerBackend( QString name, QString ext, ReadFunc rfn, WriteFunc wfn );

        /**
         * De-register a previously registered backend. If this backend is used with an instance
         * of DFL::Settings, it will remain unaffected.
         */
        static bool deregisterBackend( QString name );

        /**
         * List the registered backends. You can query the extension used by the format using
         * the function extensionForBackend(...)
         */
        static QStringList registeredBackends();

        /**
         * Obtain the extension for the register backend @backend. If @backend is not registered,
         * an empty string will be used.
         */
        static QString extensionForBackend( QString backend );

        /**
         * Set the config locations.
         * The defaults are hard coded as
         *    $HOME/.config/@P/@A/@S.conf for UserScope
         *    /etc/xdg/@p/@A/@S.conf      for DistroScope
         *    /usr/share/@p/@A/@S.conf    for SystemScope
         *
         * You can use the following place-holders:
         *   - @p/@P   will be replaced with the project name
         *   - @a/@A   will be replaced with the application name
         *   - @s/@S   will be replaced with the settings file name
         *   - ext     will be replaced with a suitable extension
         * The lower-case placeholders will be replaced with lower-cased names,
         * where as upper-case place holders will be replaces with names as-is.
         *
         * NOTE 1: There is no way to test if the path is a valid path. Providing
         * malformed paths can cause the settings object to malfunction, potentially
         * crashing the application. The users are advised to test the paths using
         * the DFL::Settings::testConfigPath function, if in doubt.
         *
         * NOTE 2: Call this function before creating any instances for these values
         * to be used.
         */
        static void setGlobalConfigPath( ConfigScope scope, QString path );

        /**
         * Test if the global config path is valid. This function will not test existence
         * of the settings file. Rather, it will test if the folder is valid and has proper
         * access permissions.
         */
        static bool testConfigPath( ConfigScope scope, QString project, QString app );

        /**
         * Set the backend for the current instance of DFL::Settings.
         * Return true of @backend is registered, else false.
         * Call this function before calling the data access functions.
         */
        bool setBackend( QString backend );

        /**
         * Set the config locations for this instance only.
         * Returns true for Distro and System scopes if path is readable.
         * Returns true for User scope if the path is both readable and writable.
         * For more details, refer to @setGlobalConfigPath(...) function.
         */
        bool setConfigPath( ConfigScope scope, QString path );

        /**
         * Some group names may be generated dynamically during runtime,
         * but have a predefined set of keys. These are called dynamic groups.
         * For example, background settings for each monitor in a multi-monitor
         * setup. With dynamic groups, one can have a skeleton group, and each
         * of the dynamic groups will map to this one:
         * # Skeleton group
         * [Background]
         * ImageUrl = ...
         * Position = ...
         *
         * Dynamic groups:
         *  - Bakground:eDP-1 -> for eDP-1 monitor
         *  - Bakground:HDMI-A-1 -> for HDMI-A-1 monitor
         *
         * Register an existing group @group as the base for dynamic groups.
         */
        void registerDynamicGroup( QString group );

        /**
         * Deregister a previously marked group as dynamic.
         */
        void deregisterDynamicGroup( QString group );

        /**
         * Sometimes, instead of dynamic groups, it may be useful to define a
         * dynamic key. This function marks a predefined key as base for dynamic keys.
         */
        void registerDynamicKey( QString key );

        /**
         * Deregister the previously mapped dynamic key.
         */
        void deregisterDynamicKey( QString dynKey );

        /**
         * Set the separator of the dynamic part of the key.
         * Default is colon (:).
         */
        void setDynamicKeySeparator( QString sep );

        /**
         * When upgrading apps/system, app and distro defaults can change.
         * Ask DFL::Settings to ignore those changes.
         */
        void setIgnoreChangesToDefault( bool );

        struct Proxy {
            QVariant value;

            operator int() const {
                return value.toInt();
            }

            operator uint() const {
                return value.toUInt();
            }

            operator float() const {
                return value.toReal();
            }

            operator double() const {
                return value.toDouble();
            }

            operator bool() const {
                return value.toBool();
            }

            operator QString() const {
                return value.toString();
            }

            operator QStringList() const {
                return value.value<QStringList>();
            }

            operator QSize() const {
                return value.toSize();
            }

            operator QFont() const {
                return value.value<QFont>();
            }

            operator QRect() const {
                return value.toRect();
            }

            operator QVariant() const {
                return value;
            }
        };

        /** Get the value of the given key. */
        Proxy value( const QString& key );

        /** Set the value of the given key */
        bool setValue( const QString& key, QVariant value );

        /** Return the list of all keys (valid only) */
        QStringList allKeys() const;

        /** Check if the settings has the given key */
        bool hasKey( const QString& key ) const;

        /** Remove the key and the corresponding key from the user settings */
        bool remove( const QString& key ) const;

    private:
        DFL::Impl::Settings *impl;

        /** Settings is scoped or not. */
        bool mScopedSettings = false;

    Q_SIGNALS:
        void settingAdded( QString, QVariant );
        void settingChanged( QString, QVariant );
        void settingRemoved( QString );

        void restartApp();
};
