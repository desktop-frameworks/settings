#include <iostream>
#include <QtCore>

#include "DFSettings.hpp"

static inline QVariantMap linearise( QVariantMap map, QString parent = QString() ) {
    QVariantMap linearMap;

    for ( QString key: map.keys() ) {
        QString newKey = (parent.size() ? parent + "::" + key : key);

        if ( (QMetaType::Type)map[ key ].type() == QMetaType::QVariantMap ) {
            linearMap.insert( linearise( map[ key ].toMap(), newKey ) );
        }

        else {
            linearMap[ newKey ] = map[ key ];
        }
    }

    return linearMap;
}


int main( int, char ** ) {
    // QVariantMap testMap;
    // testMap[ "test1" ] = "string";
    // testMap[ "test2" ] = 1;
    // testMap[ "test3" ] = 1.0;
    // testMap[ "test4" ] = QVariantList() << "string" << 1 << 1.0;
    //
    // QVariantMap level1 = {
    //     {"a", 1},
    //     {"b", 2},
    //     {"c", 3}
    // };
    //
    // QVariantMap level2 = {
    //     {"a", 4},
    //     {"b", 5},
    //     {"c", 6}
    // };
    //
    // QVariantMap level3 = {
    //     {"a", 7},
    //     {"b", 8},
    //     {"c", 9}
    // };
    //
    // level2[ "d" ] = level3;
    // level1[ "d" ] = level2;
    // testMap[ "test5" ] = level1;
    //
    // for( QString key: testMap.keys() ) {
    //     qDebug() << key << "\b:  " << testMap[ key ];
    // }
    //
    // QVariantMap linear = linearise( testMap );
    // for( QString key: linear.keys() ) {
    //     qDebug() << key << "\b:  " << linear[ key ];
    // }
    //
    // DFL::Settings testSett( "/tmp/test.ini" );
    // for( QString key: linear.keys() ) {
    //     testSett.setValue( key, linear[ key ] );
    // }

    DFL::Settings shellSett( "DesQ", "Term", "Term" );

    shellSett.setConfigPath( DFL::Settings::SystemScope, "/usr/share/@p/configs/@S.ext" );
    shellSett.setConfigPath( DFL::Settings::UserScope,   QDir::home().filePath( ".config/@P/@S.ext" ) );

    qDebug() << "";
    qDebug() << shellSett.allKeys();
    for ( QString key: shellSett.allKeys() ) {
        qDebug() << key << "\b:  " << (QVariant)shellSett.value( key );
    }
}
