/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Settings expands the QSettings class.
 *
 * IMPL Class
 *  This class will read the various settings files
 *  The class structure and working is inspired by QSettings.
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#pragma once

#include <QMap>
#include <QList>
#include <QString>
#include <QVariant>
#include <QSettings>

#include "DFSettings.hpp"

typedef struct changedsetting_t {
    /** Key whose value was changed */
    QString  key;

    /** Value corresponding to the key */
    QVariant value;

    /** What was the change: Addition (a), Deletion (d), Modification (m) */
    char     state;
} ChangedSetting;

typedef QList<ChangedSetting> ChangedSettings;

namespace DFL {
    namespace Impl {
        class Settings;
        typedef struct setting_backend_t {
            QString        name;
            QString        ext;
            DFL::ReadFunc  rFn;
            DFL::WriteFunc wFn;
        } SettingsBackend;

        typedef QMap<QString, SettingsBackend> SettingsBackends;
    }
}

class DFL::Impl::Settings : public QObject {
    Q_OBJECT;

    public:

        /**
         * @project -> Project like DesQ, Paper, Plasma, etc..
         * @app -> Settings of a specific app: Like Files, Term, Shell, etc...
         * @name -> Name of the settings file. DO NOT include an extension.
         *
         * When using non-scoped settings (i.e., using full path), set
         * project and app as empty strings.
         *
         * No extension has been set at this stage.
         * No config paths have been set at this stage.
         */
        Settings( QString project, QString app, QString name );

        /**
         * Map the dynamic group @dynGrp to an existing group @group
         * See  DFL::Settings::mapDynamicGroup(...) for more details.
         */
        void registerDynamicGroup( QString group );

        /**
         * Unmap the previously mapped dynamic group.
         * See  DFL::Settings::unmapDynamicGroup(...) for more details.
         */
        void deregisterDynamicGroup( QString group );

        /**
         * Map a dynamic key to a predefined one.
         * See  DFL::Settings::mapDynamicKey(...) for more details.
         */
        void registerDynamicKey( QString key );

        /**
         * When upgrading apps/system, app and distro defaults can change.
         * Ask DFL::Settings to ignore those changes.
         */
        void setIgnoreChangesToDefault( bool yes );

        /**
         * Unmap the previously mapped dynamic key.
         * See  DFL::Settings::unmapDynamicKey(...) for more details.
         */
        void deregisterDynamicKey( QString key );

        /** To get and set the values for a key */
        QVariant value( const QString& key );
        bool setValue( const QString& key, QVariant value );

        /** Keys */
        QStringList allKeys();
        bool hasKey( QString );
        bool remove( QString );

        /**
         * Set the settings backend
         */
        bool initializeBackend( SettingsBackend backend );

        /**
         * Setup the config paths
         */
        bool setConfigPath( DFL::Settings::ConfigScope scope, QString cfgPath );

    private:
        /** The watcher to watch for file modifications */
        QFileSystemWatcher *watcher = nullptr;

        /** The selected backend */
        SettingsBackend *backend = nullptr;

        /** Project, Application and Settings file names */
        QString mProjectName;
        QString mAppName;
        QString mSettingFileName;

        /** Config paths */
        QString defCfgPath;     // Default: /usr/share/<org>/<app>/<file>.conf
        QString sysCfgPath;     // Default: /etc/xdg/<org>/<app>/<file>.conf
        QString usrCfgPath;     // Default: ~/.config/<org>/<app>/<file>.conf

        /** Settings Maps */
        QVariantMap defSett;    // Developer defaults
        QVariantMap sysSett;    // Distro defaults
        QVariantMap usrSett;    // User settings

        /** Invalid keys in usrSett */
        QStringList mInvalidKeys;

        /** Flag to ignore changes to defaults. True by default */
        bool mIgnoreChangesToDefaults = true;

        /** Container to store all the registered backends */
        QMap<int, QString> mConfigPaths;

        /** Dynamic groups, keys and the separator */
        QStringList mDynamicGroups;
        QStringList mDynamicKeys;
        QString mDynSep = ":";

        /** Flag to mark the completion of initializations */
        bool mInitComplete = false;

        /** Initialize all the components */
        void initialize();

        /** Load the current settings. */
        void readCurrentSettings();

        /** Create the default settings backend */
        void initializeDefaultBackend();

        /** Check if the given key belongs to a dynamic group or is a dynamic key */
        bool isDynamicKey( QString );

        /**
         * Get the changed settings as a list
         * Read the current settings, then compare with the settings stored in @map.
         * Store the changed settings, and update @map.
         * Finally return the changed settings.
         */
        void getChangedSettings( QString file );

    Q_SIGNALS:
        void settingsChanged( QVariantMap added, QVariantMap changed, QStringList removed );
        void restartApp();
};
